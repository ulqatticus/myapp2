package com.example.syskonsult555.myapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syskonsult555.myapp.DBHelper.DBHelper;
import com.example.syskonsult555.myapp.utilites.Util;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Random;


// TODO add functional to change the  notes
// TODO saving flip im1age
// TODO check when save we can save text =(
public class NotesActivity extends Activity implements View.OnTouchListener, View.OnClickListener {
    TextView textView, item;
    DBHelper dbHelper;
    AlertDialog.Builder alertDialog;
    FloatingActionMenu floatingActionsMenu;
    FloatingActionButton addTextButton, addImageButton;
    ImageView image;
    SQLiteDatabase database;
    ImageButton addItemBtn, flipButton, deleteButton,savePathButton;
    // image
    File imageFile;
    /*List imagePathes;*/


    List<NoteObject> objectList;

    private static final int CAMERA_REQUEST = 1888;
    private String fName;
    private float initialX;
    private int index = 0;
    private static final int SELECT_PICTURE = 1;
    private String selectedImagePath;

    static int scale = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        initComponents();


    }

    private void initComponents() {
        dbHelper = new DBHelper(this);
        Intent intent = getIntent();
        fName = intent.getStringExtra("fname");

        LinearLayout v = (LinearLayout) findViewById(R.id.activity_notes);
        v.setOnTouchListener(this);

        textView = (TextView) findViewById(R.id.textView2);
        textView.setText(fName);

        image = (ImageView) findViewById(R.id.imageView);
        image.setOnClickListener(this);

/*        button = (Button) findViewById(R.id.btn);
        button.setOnClickListener(this);*/

        // save path button
        savePathButton = (ImageButton) findViewById(R.id.save);
        savePathButton.setOnClickListener(this);

        // add objects and then
        addItemBtn = (ImageButton) findViewById(R.id.addItem);
        addItemBtn.setOnClickListener(this);


        // delete button
        deleteButton = (ImageButton) findViewById(R.id.deleteTopic);
        deleteButton.setOnClickListener(this);

        // flip button
        flipButton = (ImageButton) findViewById(R.id.flipButton);
        flipButton.setOnClickListener(this);

        item = (TextView) findViewById(R.id.item);

        // OBJECT LIST
        objectList = dbHelper.getAllObjects(fName);

        //imagePathes = dbHelper.getPath(fName);
        if (!objectList.isEmpty()) {
            item.setText(objectList.get(0).getTextNote());
            if (objectList.get(index).getImagePathNote() != null) {
                getImage(objectList.get(0).getImagePathNote());
            } else {
                image.setImageBitmap(null);
            }
        }

        View view = findViewById(R.id.view);
        view.setOnTouchListener(this);

        floatingActionsMenu = (FloatingActionMenu) findViewById(R.id.fab_2);

        addTextButton = (FloatingActionButton) findViewById(R.id.fab_add_text);
        addTextButton.setOnClickListener(this);

        addImageButton = (FloatingActionButton) findViewById(R.id.fab_add_picture);
        addImageButton.setOnClickListener(this);

        dbHelper.close();
    }

    // screen touch
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = event.getX();

                break;
            case MotionEvent.ACTION_UP:
                float finalX = event.getX();
                // ------- next element
                if (initialX > finalX) {
                    if (!objectList.isEmpty() && index + 1 < objectList.size()) index++;
                }
                //  ------ previous element
                if (initialX < finalX) {
                    if (!objectList.isEmpty() && index > 0) index--;
                }
                break;
        }
        if (!objectList.isEmpty()) {
            if (objectList.get(index).getTextNote() != null)
                item.setText(objectList.get(index).getTextNote());
            if (objectList.get(index).getImagePathNote() != null) {
                getImage(objectList.get(index).getImagePathNote());
            } else {
                image.setImageBitmap(null);
            }
        }
        return true;

    }


    @Override
    public void onClick(View v) {
        dbHelper = new DBHelper(this);

        switch (v.getId()) {
            case R.id.fab_add_text:
                addTextItem();
                break;
            case R.id.imageView:
                if (imageFile != null) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    imageFile.getAbsoluteFile().toString();
                    intent.setDataAndType(Uri.parse("file://" +  imageFile.getAbsoluteFile().toString()), "image/*");
                    startActivity(intent);
               /*     if (isImageFitToScreen) {
                        isImageFitToScreen = false;
                        image.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        image.setAdjustViewBounds(true);
                    } else {
                        isImageFitToScreen = true;
                        image.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                        image.setScaleType(ImageView.ScaleType.FIT_XY);
                    }*/
                }
                break;
            case R.id.fab_add_picture:
                addPictureItem();
                break;
      /*      case R.id.btn:
                // display();
                if (imagePathes != null) getImageTopic((String) imagePathes.get(0));
                break;*/

            case R.id.flipButton:
                if (imageFile != null) image.setImageBitmap(flip(Util.decodeFile(imageFile)));
                break;
            case R.id.save:
             // if (imageFile != null) saveRotateImage(decodeFile(imageFile), scale);
                //TODO this add to method it will give ass an opportunity to  make and add  photo
               /* Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);*/

                break;
            case R.id.addItem:
                NoteObject noteObject = new NoteObject();
                noteObject.setImagePathNote(selectedImagePath);
                noteObject.setTextNote(textView.getText().toString());
                boolean insertData = dbHelper.saveItem(fName, noteObject);
                if (insertData) {
                    Toast.makeText(NotesActivity.this, "Success", Toast.LENGTH_LONG).show();
                    if (!objectList.isEmpty()) {
                        index = objectList.size() - 1;
                        index++;
                    }
                    objectList.add(index, noteObject);

              /*      item.setText(textView.getText().toString());
                    topicList = dbHelper.getNotes(fName);
                    index = topicList.indexOf(itemName);*/
                } else {
                    Toast.makeText(NotesActivity.this, "Error", Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.deleteTopic:
                if (!objectList.isEmpty()) {
                    dbHelper.deleteNote(objectList.get(index).getId());
                    objectList = dbHelper.getAllObjects(fName);
                    if (!objectList.isEmpty()) {
                        if (index >= 1) index--; // show previous item
                        item.setText(objectList.get(index).getTextNote());
                        if (objectList.get(index).getImagePathNote() != null) {
                            getImage(objectList.get(index).getImagePathNote());
                        } else {
                            image.setImageBitmap(null);
                        }
                    } else {
                        image.setImageBitmap(null);
                        item.setText(null);
                        Toast.makeText(NotesActivity.this, "Topic is empty now", Toast.LENGTH_LONG).show();
                    }
                } else {

                }
                break;
          /*  default:
                break;*/
        }
        dbHelper.close();
    }


    private void addTextItem() {

        alertDialog = new AlertDialog.Builder(NotesActivity.this);
        textView = new EditText(NotesActivity.this);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        textView.setLayoutParams(layoutParams);
        textView.setScroller(new Scroller(this));
        textView.setSingleLine(false);
        textView.setLines(7);
        textView.setVerticalScrollBarEnabled(true);
        textView.setMovementMethod(ScrollingMovementMethod.getInstance());
        textView.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        textView.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
        alertDialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        floatingActionsMenu.close(true);
                        item.setText(textView.getText().toString());
                        //       addItemBtn(textView.getText().toString());

                        //TODO clear this shit
                        //display();
                    }
                }
        ).setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }

                });
        alertDialog.setTitle("Add category");
        alertDialog.setView(textView);
        alertDialog.show();

    }
/*
    private void addItemBtn(String value) {
        try {
            if (value.length() != 0) {
                addData(value);
            } else {
                Toast.makeText(NotesActivity.this, "text can not be null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

        }
    }*/


    // clear this
    private void display() {
        database = dbHelper.getWritableDatabase();

        Cursor cursor = database.query(DBHelper.getTableNotes(), null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(DBHelper.getColIdNotes());
            int idName = cursor.getColumnIndex(DBHelper.getColIdNotesToCategory());
            int idActive = cursor.getColumnIndex(DBHelper.getColNameNotes());
            int imgPath = cursor.getColumnIndex(DBHelper.getPicturePathNotes());
            do {
                Log.d("nLog", "ID - " + cursor.getInt(idIndex) +
                        ", HEEEERE- " + cursor.getString(idName) +
                        ", status - " + cursor.getString(idActive) +
                        "PATH - " + cursor.getString(imgPath));
            } while (cursor.moveToNext());
        } else {
            Log.d("nLog", "0 rows");

        }
        cursor.close();
        database.close();
    }

    private void addPictureItem() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    // if image success load we save path image and number to item which we selected
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);
                //   dbHelper.savePathToImage(selectedImagePath, fName);
                image.setImageURI(selectedImageUri);
                imageFile = new File(selectedImagePath);

            }
        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            image.setImageBitmap(photo);
        }
        if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Sorry  error to load the file", Toast.LENGTH_LONG).show();
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void getImage(String path) {
        if (path != null) {
            File imgFile = new File(path);
            Log.d("nLog", " --- " + path);
            if (imgFile.exists()) {
                try {
                    imageFile = new File(path);
                    image.setImageBitmap(Util.decodeFile(imageFile));
                } catch (Exception e) {
                    Toast.makeText(this, "Sorry  error to load the file", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            image.setImageDrawable(null);
            Toast.makeText(this, "file not exist", Toast.LENGTH_LONG).show();
        }
    }


    // rotate image
    public static Bitmap flip(Bitmap src) {
        // create new matrix for transformation
        Matrix matrix = new Matrix();
        // rotate on 90 degree
        scale += 90;
        matrix.postRotate(scale);
        // return transformed image
        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    private void saveRotateImage(Bitmap finalBitmap, int scale) {
        Matrix matrix = new Matrix();
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fileName = "Image-" + n + ".jpg";
        File file = new File(myDir, fileName);
        //TODO rename file if exist
        if (file.exists()) file.delete();
        try {
            matrix.postRotate(scale);
            FileOutputStream out = new FileOutputStream(file);
            Bitmap rotatedBitmap = Bitmap.createBitmap(finalBitmap, 0, 0, finalBitmap.getWidth(), finalBitmap.getHeight(), matrix, true);
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
