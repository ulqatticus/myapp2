package com.example.syskonsult555.myapp.listeners;

/**
 * Created by atticus on 16.05.17.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syskonsult555.myapp.DBHelper.DBHelper;
import com.example.syskonsult555.myapp.NotesActivity;
import com.example.syskonsult555.myapp.R;
import com.example.syskonsult555.myapp.TopicObject;
import com.example.syskonsult555.myapp.utilites.Util;
import java.io.File;
import java.util.ArrayList;

// TODO optimization
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private ArrayList<TopicObject> topicObjects;
    private File imageFile;
    private DBHelper dbHelper;

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener, PopupMenu.OnMenuItemClickListener {
        private TextView nameTopic;
        private ImageView image;


        public ViewHolder(final View view) {
            super(view);
            dbHelper = new DBHelper(view.getContext());
            nameTopic = (TextView) view.findViewById(R.id.topicName);
            image = (ImageView) view.findViewById(R.id.topicPhoto);

            // set imagepath into listobject- it's first image in topic
            for (int i = 0; i < topicObjects.size(); i++) {
                topicObjects.get(i).setImageTopic(dbHelper.getImagePathes(topicObjects.get(i).getNameTopic()));
            }

            //  descriptionTopic = (TextView) v.findViewById(R.id.topicDescription);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position = getAdapterPosition();
                    final String selectedItem = topicObjects.get(position).getNameTopic();
                    Snackbar.make(v, "Click detected on item " + selectedItem, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    Intent myIntent = new Intent(view.getContext(), NotesActivity.class);
                    myIntent.putExtra("fname", selectedItem);
                    view.getContext().startActivity(myIntent);

                }
            });
            view.setOnCreateContextMenuListener(this);

        }

        //Snackbar.make(v, "Click detected on item " + position, Snackbar.LENGTH_LONG).setAction("Action", null).show();
        // update list
        public void updateList(ArrayList<TopicObject> items) {
            if (items != null && items.size() > 0) {
                topicObjects.clear();
                topicObjects.addAll(items);
                for (int i = 0; i < topicObjects.size(); i++) {
                    topicObjects.get(i).setImageTopic(dbHelper.getImagePathes(topicObjects.get(i).getNameTopic()));
                }
                notifyDataSetChanged();
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            final CharSequence topicName = this.nameTopic.getText();
            switch (item.getItemId()) {
                case R.id.edit:
                    changeTopic(String.valueOf(topicName));
                    break;

                case R.id.delete:
                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(itemView.getContext());
                    alt_bld.setMessage("Do you want to delete this item?")
                            .setPositiveButton("Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dbHelper.deleteTopic(String.valueOf(topicName));
                                            ArrayList newTopicObjects = dbHelper.getTopicObjects();
                                            updateList(newTopicObjects);
                                        }
                                    }
                            ).setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });

                    alt_bld.show();
                    break;
            }
            dbHelper.close();
            return true;
        }

        private void changeTopic(String name) {
            final Dialog dialog = new Dialog(itemView.getContext());
            final String topicId = dbHelper.getTopicId(name);
            dialog.setContentView(R.layout.change_name);
            dialog.setTitle("Change name");
            dialog.setCancelable(true);
            final EditText editText = (EditText) dialog.findViewById(R.id.text_name);
            editText.setText(name);
            editText.setSelection(editText.getText().length());
            final Button cancelButton = (Button) dialog.findViewById(R.id.cancel_btn);
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            final Button sendButton = (Button) dialog.findViewById(R.id.confirm_btn);

            sendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkOnDuplicateNameTopic(editText.getText().toString())) {
                        dbHelper.changeTopic(editText.getText().toString(), topicId);
                        dialog.dismiss();
                        ArrayList newTopicObjects = dbHelper.getTopicObjects();
                        updateList(newTopicObjects);

                    } else {
                        Toast.makeText(itemView.getContext(), "duplicate name", Toast.LENGTH_LONG).show();
                    }
                }
            });
            dialog.show();
        }


        private boolean checkOnDuplicateNameTopic(String s) {
            if (!topicObjects.isEmpty()) {
                for (TopicObject topic : topicObjects) {
                    if (topic.getNameTopic().equals(s)) {
                        return false;
                    }
                }
            }
            return true;
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            PopupMenu popup = new PopupMenu(v.getContext(), v);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.menu, popup.getMenu());
            popup.setOnMenuItemClickListener(this);
            popup.show();
        }
    }


    public RecyclerAdapter(ArrayList<TopicObject> dataset) {
        topicObjects = dataset;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemView.setLongClickable(true);
        return viewHolder;
    }




    private File getImageFile(String path) {

        if (path != null) {
            File imgFile = new File(path);
            if (imgFile.exists()) {
                try {
                    imageFile = new File(path);
                } catch (Exception e) {
                    Log.d("err","EXCEPTION - "+e);
                }
            }
        } else {
            imageFile = null;
        }
        return imageFile;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      //  TimingLogger timings = new TimingLogger("time", "methodA");

        holder.nameTopic.setText(topicObjects.get(position).getNameTopic());
        if (topicObjects.get(position).getImageTopic() != null)
            holder.image.setImageBitmap(Util.decodeFile(getImageFile(topicObjects.get(position).getImageTopic())));
        //  holder.descriptionTopic.setText(mDataset.get(position).getDescriptionTopic());
    /*    timings.addSplit("work A");
        timings.dumpToLog();
        Log.v("time","this is time " +timings.toString() );*/
    }

    @Override
    public int getItemCount() {
        return topicObjects.size();
    }

}