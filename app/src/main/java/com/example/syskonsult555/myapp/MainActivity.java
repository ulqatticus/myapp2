package com.example.syskonsult555.myapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;
import com.example.syskonsult555.myapp.DBHelper.DBHelper;
import com.example.syskonsult555.myapp.listeners.RecyclerAdapter;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    /*   Button   displayBtn;*/
   // EditText categoryTextEdit;
    ArrayList<TopicObject> topicObjects; // main list of objects

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    DBHelper dbHelper;
    FloatingActionMenu floatingActionsMenu;
    FloatingActionButton actionButton, clearButton;
    AlertDialog.Builder alertDialog;
    SQLiteDatabase database;
    EditText input;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initComponents();
    }

    private void initComponents() {
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            //getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setTitle("Atticuss");
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().hide();
        }

        dbHelper = new DBHelper(this);
        floatingActionsMenu = (FloatingActionMenu) findViewById(R.id.fab);
        actionButton = (FloatingActionButton) findViewById(R.id.fab_add);
        actionButton.setOnClickListener(this);

        clearButton = (FloatingActionButton) findViewById(R.id.fab_clear);
        clearButton.setOnClickListener(this);

        topicObjects = dbHelper.getTopicObjects();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RecyclerAdapter(topicObjects);
        recyclerView.setAdapter(adapter);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final String name = ((TextView) info.targetView).getText().toString();

        switch (item.getItemId()) {
            case R.id.delete:
                AlertDialog.Builder alt_bld = new AlertDialog.Builder(MainActivity.this);
                alt_bld.setMessage("Do you want to delete this item?")
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dbHelper.deleteTopic(name);
                                        //      displayInfoInList();
                                    }
                                }
                        ).setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });

                alt_bld.show();
                break;

            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        database = dbHelper.getWritableDatabase();
        switch (v.getId()) {
           /* case R.id.display_button:
                Cursor cursor = database.query(DBHelper.getTableCategory(), null, null, null, null, null, null);
                if (cursor.moveToFirst()) {
                    int idIndex = cursor.getColumnIndex(DBHelper.getColId());
                    int idName = cursor.getColumnIndex(DBHelper.getColName());
                    int idActive = cursor.getColumnIndex(DBHelper.getColActive());
                    do {
                        Log.d("nLog", "ID - " + cursor.getInt(idIndex) +
                                ", name - " + cursor.getString(idName) +
                                ", status - " + cursor.getInt(idActive));
                    } while (cursor.moveToNext());
                } else {
                    Log.d("nLog", "0 rows");

                }
                cursor.close();
                break;*/
            case R.id.fab_clear:
                database.delete(DBHelper.getTableCategory(), null, null);
                database.delete(DBHelper.getTableNotes(), null, null);
                topicObjects.clear();
                break;
            case R.id.fab_add:
                addTopic();
                break;
        }

        dbHelper.close();

    }

    private void addTopic() {
        alertDialog = new AlertDialog.Builder(MainActivity.this);
        input = new EditText(MainActivity.this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(layoutParams);
        input.setScroller(new Scroller(this));
        input.setSingleLine(false);
        input.setLines(3);
        input.setVerticalScrollBarEnabled(true);
        input.setMovementMethod(ScrollingMovementMethod.getInstance());
        alertDialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (checkOnDuplicateNameTopic(input.getText().toString())) {
                            floatingActionsMenu.close(true);
                            addCategory(input.getText().toString());
                            topicObjects = dbHelper.getTopicObjects();
                            adapter = new RecyclerAdapter(topicObjects);
                            recyclerView.setAdapter(adapter);
                            // displayInfoInList();
                        } else {
                            Toast.makeText(MainActivity.this, "duplicate name", Toast.LENGTH_LONG).show();

                        }
                    }
                }
        ).setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }

                });
        alertDialog.setTitle("Add category");
   /*     alertDialog.setMessage("Enter the name");*/
        alertDialog.setView(input);
        alertDialog.show();

    }

    private boolean checkOnDuplicateNameTopic(String s) {
        if (!topicObjects.isEmpty()) {
            for (TopicObject name : topicObjects) {
                if (name.getNameTopic().equals(s)) {
                    return false;
                }
            }
        }
        return true;
    }

    private void addCategory(String value) {
        try {
            if (value.length() != 0) {
                addData(value.toString());
            } else {
                Toast.makeText(MainActivity.this, "text can not be null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

        }
    }

    private void addData(String category_name) {
        boolean insertData = dbHelper.addTopic(category_name);
        if (insertData) {
            Toast.makeText(MainActivity.this, "success", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "wrong", Toast.LENGTH_LONG).show();

        }
    }
    @Override
    protected void onDestroy() {super.onDestroy();}

    @Override
    protected void onPause() {super.onPause();}

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }
}



