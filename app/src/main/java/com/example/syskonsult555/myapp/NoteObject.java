package com.example.syskonsult555.myapp;

/**
 * Created by atticus on 05.05.17.
 */

public class NoteObject {


    private String id;
    private String textNote;
    private String imagePathNote;


    public String getId() {return id;}

    public void setId(String id) {this.id = id;}

    public String getTextNote() {
        return textNote;
    }

    public void setTextNote(String textNote) {
        this.textNote = textNote;
    }

    public String getImagePathNote() {
        return imagePathNote;
    }

    public void setImagePathNote(String imagePathNote) {
        this.imagePathNote = imagePathNote;
    }
}
