package com.example.syskonsult555.myapp.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.syskonsult555.myapp.NoteObject;
import com.example.syskonsult555.myapp.TopicObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by atticus on 14.02.17.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 2;
    private static final String DB_NAME = "remember";

    // create table  category
    private static final String TABLE_CATEGORY = "category";
    private static final String COL_ID = "_id";
    private static final String COL_NAME = "name";
    private static final String COL_ACTIVE = "active";
    //  create table notes
    private static final String TABLE_NOTES = "notes";
    private static final String COL_ID_NOTES = "_id";
    private static final String COL_ID_NOTES_TO_CATEGORY = "id_category";
    private static final String COL_NAME_NOTES = "name";
    private static final String PICTURE_PATH_NOTES = "picture_path";


    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " +
                TABLE_CATEGORY + "(" +
                COL_ID + " integer primary key," +
                COL_NAME + " text," +
                COL_ACTIVE + " integer" + ")");

        db.execSQL("create table " +
                TABLE_NOTES + "(" +
                COL_ID_NOTES + " integer primary key," +
                COL_NAME_NOTES + " text," +
                PICTURE_PATH_NOTES + " text," +
                COL_ID_NOTES_TO_CATEGORY + " integer, " +
                " FOREIGN KEY (" + COL_ID_NOTES_TO_CATEGORY + ") REFERENCES " + TABLE_CATEGORY + "(" + COL_ID + "));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_CATEGORY);
        db.execSQL("drop table if exists " + TABLE_NOTES);
        onCreate(db);
    }

    public boolean addTopic(String item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME, item);
        long result = db.insert(TABLE_CATEGORY, null, contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;

        }
    }

/*    public Cursor getList() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("SELECT * FROM " + TABLE_CATEGORY, null);

        return data;
    }*/

    // delete topic
    public void deleteTopic(String itemToDelete) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            // delete all notes which  were connected to item
            db.execSQL("DELETE FROM " + TABLE_NOTES + " WHERE " + COL_ID_NOTES_TO_CATEGORY + " = (SELECT " + COL_ID + " FROM category  WHERE " + COL_NAME + " = '" + itemToDelete + "')");
            // delete  item
            db.execSQL("DELETE FROM " + TABLE_CATEGORY + " WHERE " + COL_NAME + " = '" + itemToDelete + "'");
            db.close();
        } catch (Exception e) {
            Log.e("DB ERROR", e.toString());
            e.printStackTrace();
        }
    }

 /*   public LinkedList<String> getNotes(String topic) {
        LinkedList itemList = new LinkedList();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("SELECT " + COL_NAME_NOTES + " FROM " + TABLE_NOTES + " WHERE " + COL_ID_NOTES_TO_CATEGORY + " = (SELECT " + COL_ID + " FROM category  WHERE " + COL_NAME + " = '" + topic + "')", null);
        while (data.moveToNext()) {
            itemList.add(data.getString(0));
        }
        db.close();
        data.close();
        return itemList;

    }*/

    public boolean saveItem(String topic, NoteObject noteObject) {
        LinkedList itemList = new LinkedList();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("SELECT " + COL_ID + " FROM " + TABLE_CATEGORY + " WHERE " + COL_NAME + " = '" + topic + "'", null);
        while (data.moveToNext()) {
            itemList.add(data.getString(0));
        }
        ContentValues row = new ContentValues();
        if (noteObject.getImagePathNote() != null)
            row.put(PICTURE_PATH_NOTES, noteObject.getImagePathNote());
        if (noteObject.getTextNote() != null) row.put(COL_NAME_NOTES, noteObject.getTextNote());
        row.put(COL_ID_NOTES_TO_CATEGORY, itemList.get(0).toString());
        long result = db.insert(TABLE_NOTES, null, row);

        if (result == -1) {
            return false;
        } else {
            return true;

        }
    }

    public List<NoteObject> getAllObjects(String fName) {
        // Get the isntance of the database
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NOTES + " WHERE " + COL_ID_NOTES_TO_CATEGORY + " = (SELECT " + COL_ID + " FROM category  WHERE " + COL_NAME + " = '" + fName + "')";
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<NoteObject> objectList = new ArrayList<>();
        try {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    NoteObject object = new NoteObject();
                    object.setId(cursor.getString(0));
                    object.setTextNote(cursor.getString(1));
                    object.setImagePathNote(cursor.getString(2));
                    // Adding to list
                    objectList.add(object);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.d("dbErr", e.getMessage());
            return null;
        } finally {
            //release all your resources
            cursor.close();
            db.close();
        }
        return objectList;
    }

    public void deleteNote(String index) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            // delete  item
            db.execSQL("DELETE FROM " + TABLE_NOTES + " WHERE " + COL_ID_NOTES + " = '" + index + "'");
            db.close();
        } catch (Exception e) {
            Log.e("dbErr", e.toString());
            e.printStackTrace();
        }
    }

    public void changeTopic(String name, String id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(COL_NAME, name); //These Fields should be your String values of actual column names
            db.update(TABLE_CATEGORY, cv, "_id=" + id, null);
        } catch (SQLiteException e) {

        }
    }

    public String getTopicId(String name) {

        LinkedList<String> itemList = new LinkedList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("SELECT " + COL_ID + " FROM " + TABLE_CATEGORY + " WHERE " + COL_NAME + " = '" + name + "'", null);
        while (data.moveToNext()) {
            itemList.add(data.getString(0));
        }

        return itemList.get(0);


    }

    // if we have item
    public boolean checkOnExist() {

        return true;
    }

    // get first images   from  items for recyclerview
    public String getImagePathes(String nameTopic) {
        LinkedList<String> itemList = new LinkedList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + PICTURE_PATH_NOTES + " FROM " + TABLE_NOTES + " WHERE " + COL_ID_NOTES_TO_CATEGORY + " = (SELECT " + COL_ID + " FROM category  WHERE " + COL_NAME + " = '" + nameTopic + "')", null);
        try {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    // Adding to list
                    itemList.add(cursor.getString(0));
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.d("dbErr", e.getMessage());
            return null;
        } finally {
            //release all your resources
            cursor.close();
            db.close();
        }
        if (!itemList.isEmpty()) {
            return itemList.get(0);
        } else
            return null;
    }

    public ArrayList<TopicObject> getTopicObjects() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_CATEGORY;
        Cursor cursor = db.rawQuery(query, null);
        ArrayList<TopicObject> objectList = new ArrayList<>();
        try {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    TopicObject object = new TopicObject();
                    object.setIdTopic(cursor.getString(0));
                    object.setNameTopic(cursor.getString(1));
                    // object.setImagePathNote(cursor.getString(2));
                    // Adding to list
                    objectList.add(object);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.d("dbErr", e.getMessage());
            return null;
        } finally {
            //release all your resources
            cursor.close();
            db.close();
        }
        return objectList;
    }

    public static String getPicturePathNotes() {
        return PICTURE_PATH_NOTES;
    }

    public static String getColIdNotesToCategory() {
        return COL_ID_NOTES_TO_CATEGORY;
    }

    public static String getColNameNotes() {
        return COL_NAME_NOTES;
    }

    public static String getColIdNotes() {
        return COL_ID_NOTES;
    }

    public static String getTableNotes() {
        return TABLE_NOTES;
    }

    public static String getTableCategory() {
        return TABLE_CATEGORY;
    }


}