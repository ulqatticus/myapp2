package com.example.syskonsult555.myapp.utilites;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by atticus on 02.06.17.
 */

public class Util {
    public static Bitmap decodeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, options);

            // The new size we want to scale to
            final int REQUIRED_SIZE = 320;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options options1 = new BitmapFactory.Options();
            options1.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, options1);
        } catch (FileNotFoundException e) {
            //  Toast.makeText(NotesActivity.this, "File not found", Toast.LENGTH_LONG).show();
        }
        return null;
    }
}
