package com.example.syskonsult555.myapp;

/**
 * Created by atticus on 16.05.17.
 */

public class TopicObject {


    private String idTopic;
    private String nameTopic;
    private String descriptionTopic;
    private String imageTopic;

    public String getIdTopic() {
        return idTopic;
    }

    public void setIdTopic(String idTopic) {
        this.idTopic = idTopic;
    }

    public String getNameTopic() {
        return nameTopic;
    }

    public void setNameTopic(String nameTopic) {
        this.nameTopic = nameTopic;
    }

    public String getDescriptionTopic() {
        return descriptionTopic;
    }

    public void setDescriptionTopic(String descriptionTopic) {
        this.descriptionTopic = descriptionTopic;
    }

    public String getImageTopic() {
        return imageTopic;
    }

    public void setImageTopic(String imageTopic) {
        this.imageTopic = imageTopic;
    }



}
